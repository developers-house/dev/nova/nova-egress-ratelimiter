load(
    "@envoy//bazel:envoy_build_system.bzl",
    "envoy_cc_binary",
    "envoy_cc_library",
)
load("@envoy_api//bazel:api_build_system.bzl", "api_proto_package")

package(default_visibility = ["//visibility:public"])

# Builds for the envoy binary
envoy_cc_binary(
    name = "envoy",
    repository = "@envoy",
    deps = [
        ":nova_egress_ratelimiter_config",
        "@envoy//source/exe:envoy_main_entry_lib",
    ],
)

# Builds for the nova_egress_ratelimiter_config package
envoy_cc_library(
    name = "nova_egress_ratelimiter_config",
    srcs = ["nova-egress-ratelimiter-config.cc"],
    repository = "@envoy",
    deps = [
        ":nova_egress_ratelimiter_lib",
        "@envoy//envoy/server:filter_config_interface",
    ],
)

# Builds the api proto files for the configuration library
api_proto_package()

# Builds for the nova_egress_ratelimiter_libs package
envoy_cc_library(
    name = "nova_egress_ratelimiter_lib",
    srcs = ["nova-egress-ratelimiter.cc"],
    hdrs = ["nova-egress-ratelimiter.hpp"],
    repository = "@envoy",
    deps = [
        ":pkg_cc_proto",
        "@envoy//source/extensions/filters/http/common:pass_through_filter_lib",
    ],
)